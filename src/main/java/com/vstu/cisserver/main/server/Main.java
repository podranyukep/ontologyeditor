/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vstu.cisserver.main.server;

import com.complexible.stardog.api.Connection;
import com.complexible.stardog.api.ConnectionConfiguration;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.vstu.cisserver.main.server.servlets.SimpleServlet;

/**
 *
 * @author Katie
 */
public class Main {
    
    /*
    * Параметры подключения к базе данных
    */
    private static String dbname = "TestDatabase";
    private static String username = "admin";
    private static String userpswd = "admin";
    private static String serverlink = "http://109.206.173.225:5820/";
    
    
    public static void main(String[] args) throws Exception{
        
        // Пытаемся создать подключение к базе данных
        try (Connection aConn = ConnectionConfiguration
                .to(dbname)
                .credentials(username, userpswd)
                .server(serverlink)
                .connect())
        {
            System.out.println("Connected to DB");
            // Инициализируем сервлеты
        
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

            context.addServlet(new ServletHolder(new SimpleServlet(aConn)), "/index");

            ResourceHandler resource_handler = new ResourceHandler();
            // TODO исправить директорию
            resource_handler.setResourceBase("C:\\CISServer\\pages");

            HandlerList handlers = new HandlerList();
            handlers.setHandlers(new Handler[]{resource_handler, context});


            Server server = new Server(8000);
            server.setHandler(handlers);

            try {
                server.start(); 
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.println("Server started");

            try { 
                server.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception e)
        {
            // Если произошло исключение, пробрасываем его наверх с формированным текстом ошибки
            String errorMessage = "Во время подключения к базе данных произошла ошибка: \n"+
                    e.getMessage() + "\nПожалуйста, проверьте настройки и повторите позднее.";
            throw new Exception(errorMessage);
        }
    }
}
