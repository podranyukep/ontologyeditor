/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vstu.cisserver.main.server.servlets;

import com.google.gson.JsonObject;
import javax.servlet.http.HttpServlet;
import com.complexible.stardog.api.Connection;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Katie
 */
@WebServlet(name = "SimpleServlet", urlPatterns = "/index")
public class SimpleServlet extends HttpServlet {
    
    private final Connection dbConn;
    
    public SimpleServlet(Connection dbConn){
        this.dbConn = dbConn;
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        String json;
        JsonObject object = new JsonObject();
        object.addProperty("status", "everything is ok"); 
        json = object.toString();
        resp.getWriter().println(json);
        resp.setStatus(HttpServletResponse.SC_OK);
    }
}
